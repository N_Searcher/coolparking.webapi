﻿using System;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MenuLib;
using System.Linq;
using System.Collections.Generic;

namespace Cool_Parking
{
    class Program
    {
        static IParkingService _parkingService;

        static void Main(string[] args)
        {
            ConsoleMenu mainMenu = new(new string[] { "Show parking info", "Add Vehicle", "Remove Vehicle", "Add Money" });

            InitializeParking();

            do
            {
                switch (mainMenu.ShowStartMenu(true))
                {
                    case 0:
                        ShowParkingInfo();
                        break;
                    case 1:
                        AddVehicle();
                        break;
                    case 2:
                        RemoveVehicle();
                        break;
                    case 3:
                        TopUpVehicleBalance();
                        break;
                }
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }

        static void InitializeParking()
        {
            var logTimer = new TimerService() { Interval = Settings.LoggingPeriod * 1000 };
            var withdrawTimer = new TimerService() { Interval = Settings.PaymentWriteOffInterval * 1000 };

            _parkingService = new ParkingService(logTimer, withdrawTimer, new LogService("logFile.txt"));
        }

        static void ShowParkingInfo()
        {
            Console.WriteLine($"Current parking balance: {_parkingService.GetBalance()}");
            Console.WriteLine($"Free places: {_parkingService.GetFreePlaces()}");
            Console.WriteLine($"Current earned money : {_parkingService.GetLastParkingTransactions().Sum(item => item.Sum)}");
        }

        static void ShowAllTransactions()
        {
            Console.Write(_parkingService.ReadFromLog());
        }

        static void AddVehicle()
        {
            ConsoleMenu vehicleTypes = new(new[] { "PassengerCar", "Truck", "Bus", "Motorcycle" });

            Console.WriteLine("Adding vehicle");
            Console.WriteLine("Choose vehicle type: ");

            var type = VehicleType.Bus;

            switch (vehicleTypes.ShowStartMenu(false))
            {
                case 0:
                    type = VehicleType.PassengerCar;
                    break;
                case 1:
                    type = VehicleType.Truck;
                    break;
                case 2:
                    type = VehicleType.Bus;
                    break;
                case 3:
                    type = VehicleType.Motorcycle;
                    break;
            }

            Console.Write("Put some money: ");
            var money = Console.ReadLine();

            _parkingService.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), type, decimal.Parse(money)));
        }

        static void RemoveVehicle()
        {
            var vehicles = _parkingService.GetVehicles();
            var menuItems = new List<string>();
            ConsoleMenu vehiclesIDs;

            foreach (var item in vehicles)
            {
                menuItems.Add(item.Id);
            }

            vehiclesIDs = new ConsoleMenu(menuItems.ToArray());

            Console.WriteLine("Remove vehicle from parking: ");

            _parkingService.RemoveVehicle(menuItems[vehiclesIDs.ShowStartMenu()]);
        }

        static void TopUpVehicleBalance()
        {
            Console.WriteLine("Topping up vehicle's balance");
            Console.WriteLine("Choose vehicle to top up:");
            var vehicles = _parkingService.GetVehicles();
            var menuItems = new List<string>();
            ConsoleMenu vehiclesIDs;

            foreach (var item in vehicles)
            {
                menuItems.Add(item.Id);
            }

            vehiclesIDs = new ConsoleMenu(menuItems.ToArray());

            int vehicleID = vehiclesIDs.ShowStartMenu();

            Console.Write("Enter sum: ");

            _parkingService.TopUpVehicle(menuItems[vehicleID], decimal.Parse(Console.ReadLine()));
        }
    }
}
