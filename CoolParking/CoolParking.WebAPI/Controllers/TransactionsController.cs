﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using System;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly Services.WebParkingService _webParking;

        public TransactionsController()
        {
            _webParking = new Services.WebParkingService();
        }

        [HttpGet("last")]
        public ActionResult<TransactionInfo> GetLastTransactions()
        {
            return Ok(_webParking.WebParking.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(_webParking.WebParking.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle(string id, decimal sum)
        {
            if (sum < 0)
                return BadRequest();
            try
            {
                _webParking.WebParking.TopUpVehicle(id, sum);
                return Ok(_webParking.GetVehicleById(id).Item1);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
        }
    }
}
