﻿using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("/api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly Services.WebParkingService _webParking;

        public ParkingController()
        {
            _webParking = new Services.WebParkingService();
        }

        [HttpGet("balance")]
        public ActionResult<decimal> Balance()
        {
            return Ok(_webParking.WebParking.GetBalance());
        }

        [HttpGet("capacity")]
        public ActionResult<int> Capacity()
        {
            return Ok(_webParking.WebParking.GetCapacity());
        }

        [HttpGet("freePlaces")]
        public ActionResult<int> FreePlaces()
        {
            return Ok(_webParking.WebParking.GetFreePlaces());
        }
    }
}
