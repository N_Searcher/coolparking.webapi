﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CoolParking.BL.Models;
using System;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly Services.WebParkingService _webParking;

        public VehiclesController()
        {
            _webParking = new Services.WebParkingService();
        }

        [HttpGet]
        public ActionResult<IReadOnlyCollection<Vehicle>> Get()
        {
            return Ok(_webParking.WebParking.GetVehicles());
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            (Vehicle vehicle, string status) vehicle = _webParking.GetVehicleById(id);

            if (vehicle.status == "400")
                return BadRequest();
            if (vehicle.status == "404")
                return NotFound();

            return Ok(vehicle.vehicle);
        }

        [HttpPost]
        public ActionResult Post(string id, int type, decimal balance)
        {
            try
            {
                var newVehicle = new Vehicle(id, (VehicleType)type, balance);
                _webParking.WebParking.AddVehicle(newVehicle);
                return Created("", newVehicle);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public ActionResult Delete(string id)
        {
            (Vehicle vehicle, string status) vehicle = _webParking.GetVehicleById(id);

            if (vehicle.status == "400")
                return BadRequest();
            if (vehicle.status == "404")
                return NotFound();

            _webParking.WebParking.RemoveVehicle(id);
            return NoContent();
        }
    }
}
