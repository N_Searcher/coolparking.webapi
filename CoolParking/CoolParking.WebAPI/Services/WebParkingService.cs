﻿using System.Linq;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Services
{
    public class WebParkingService
    {
        public ParkingService WebParking { get; set; }

        public WebParkingService()
        {
            var withdrawInterval = new TimerService() { Interval = Settings.PaymentWriteOffInterval * 1000 };
            var logInterval = new TimerService() { Interval = Settings.LoggingPeriod * 1000 };
            WebParking = new ParkingService(withdrawInterval, logInterval, new LogService("logFile.txt"));
        }

        public (Vehicle, string) GetVehicleById(string id)
        {
            var template = new Regex(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

            if (!template.IsMatch(id))
                return (null, "400");

            List<Vehicle> vehicles = WebParking.GetVehicles().Where(item => item.Id == id).ToList();

            if (vehicles.Count == 0)
                return (null, "404");

            return (vehicles[0], "200");
        }
    }
}
