﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }
        public string Id { get; private set; }

        public Vehicle(string vehicleID, VehicleType vehicleType, decimal balance)
        {
            if (!CheckID(vehicleID))
                throw new ArgumentException("Wrong \"Vehicle Id\" format!");
            if(balance < 0)
                throw new ArgumentException("Wrong balance sum");

            Id = vehicleID;
            VehicleType = vehicleType;
            Balance = balance;
        }

        private static bool CheckID(string id)
        {
            var template = new Regex(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

            return template.IsMatch(id);
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rand = new Random();

            string id = "";

            for (var i = 0; i < 10; i++)
            {
                if (i < 2 || i > 7)
                    id += Convert.ToChar(rand.Next(65, 91)).ToString();
                else if (i == 2 || i == 7)
                    id += "-";
                else
                    id += rand.Next(0, 10).ToString();
            }

            return id;
        }
    }
}