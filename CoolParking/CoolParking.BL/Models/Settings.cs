﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance { get; set; }
        public static int ParkingCapacity { get; set; }
        public static int PaymentWriteOffInterval { get; set; }
        public static int LoggingPeriod { get; set; }
        public static decimal Multyplier { get; set; }
        public static Dictionary<VehicleType, decimal> Tariffs { get; set; }

        static Settings()
        {
            StartBalance = 0;
            ParkingCapacity = 10;
            PaymentWriteOffInterval = 5;
            LoggingPeriod = 60;
            Multyplier = 2.5m;
            Tariffs = new Dictionary<VehicleType, decimal>()
            {
                { VehicleType.Bus, 3.5m},
                { VehicleType.Motorcycle, 1m},
                { VehicleType.PassengerCar, 2m},
                { VehicleType.Truck, 5m }
            };
        }
    }
}
