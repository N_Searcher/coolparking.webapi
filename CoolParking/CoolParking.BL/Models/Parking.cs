﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static readonly Parking _parking = new();

        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        public int ParkingCapacity { get; set; }
        public int FreePlaces
        {
            get
            {
                return ParkingCapacity - Vehicles.Count;
            }
        }

        public static Parking GetInstance()
        {
            return _parking;
        }

        private Parking()
        {
            Vehicles = new();
            Balance = Settings.StartBalance;
            ParkingCapacity = Settings.ParkingCapacity;
        }
    }
}
