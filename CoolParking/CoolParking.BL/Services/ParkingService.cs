﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Interfaces;
using System.Timers;
using System.Collections.Generic;

namespace CoolParking.BL.Services
{
    public class ParkingService : Interfaces.IParkingService
    {
        private readonly Parking parking = Parking.GetInstance();
        private readonly ILogService _logService;
        private readonly ITimerService _logTimer;
        private readonly ITimerService _withdrawTimer;
        private readonly List<TransactionInfo> _transactions;

        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            parking.ParkingCapacity = Settings.ParkingCapacity;
            this._logService = _logService;
            this._logTimer = _logTimer;
            this._withdrawTimer = _withdrawTimer;
            _transactions = new();
            this._withdrawTimer.Elapsed += OnWithdrawEvent;
            this._logTimer.Elapsed += OnLoggingEvent;
            this._withdrawTimer.Start();
            this._logTimer.Start();
        }

        public void OnWithdrawEvent(object sender, ElapsedEventArgs args)
        {
            foreach (var item in parking.Vehicles)
            {
                item.Balance = WithdrawPayment(item.Balance, Settings.Tariffs[item.VehicleType]);
                _transactions.Add(new TransactionInfo(item.Id, Settings.Tariffs[item.VehicleType]));
            }
        }

        public void OnLoggingEvent(object sender, ElapsedEventArgs args)
        {
            foreach (var item in _transactions)
            {
                _logService.Write($"{item.TransactionTime} {item.VehicleId} {item.Sum}");
            }

            _transactions.Clear();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.Vehicles.Count == parking.ParkingCapacity)
                throw new InvalidOperationException("Parking is full");
            if (parking.Vehicles.Where(item => string.Compare(item.Id, vehicle.Id) == 0).Any())
                throw new ArgumentException("Vehicle is already in the parking!");

            parking.Vehicles.Add(vehicle);
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return parking.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return parking.FreePlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = parking.Vehicles.Where(item => string.Compare(item.Id, vehicleId) == 0).ToList();

            if (vehicle.Count == 0)
                throw new ArgumentException("There are no vehicles on the parking");
            if (!parking.Vehicles.Remove(vehicle[0]))
                throw new ArgumentException("There is no such vehicle");
            if (vehicle[0]?.Balance < 0)
                throw new InvalidOperationException("Your ride owns me some money");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = parking.Vehicles.Where(item => string.Compare(item.Id, vehicleId) == 0).ToList();

            if (vehicle.Count == 0)
                throw new ArgumentException("There is no such vehicle");
            if (sum < 0)
                throw new ArgumentException("Sum is lesser then zero");

            vehicle[0].Balance += sum;
        }

        public void Dispose()
        {
            parking.ParkingCapacity = Settings.ParkingCapacity;
            parking.Vehicles.Clear();
            parking.Balance = 0;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        private decimal WithdrawPayment(decimal vehicleBalance, decimal withdrawValue)
        {
            if (vehicleBalance < withdrawValue)
            {
                if (vehicleBalance < 0)
                {
                    vehicleBalance -= withdrawValue * 2.5m;
                    parking.Balance += withdrawValue * 2.5m;
                }
                else
                {
                    vehicleBalance -= (withdrawValue - vehicleBalance) * 2.5m + vehicleBalance;
                    parking.Balance += (withdrawValue - vehicleBalance) * 2.5m + vehicleBalance;
                }
            }
            else
            {
                vehicleBalance -= withdrawValue;
                parking.Balance += withdrawValue;
            }

            return vehicleBalance;
        }

    }
}