﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : Interfaces.ILogService
    {
        public string LogPath { get; private set; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("There is no such file");

            string log;

            using (var fileReader = new StreamReader(LogPath))
            {
                log = fileReader.ReadToEnd();
            }

            return log;
        }

        public void Write(string logInfo)
        {
            using (var fileWriter = new StreamWriter(LogPath, true))
            {
                fileWriter.WriteLine(logInfo);
            }
        }
    }
}