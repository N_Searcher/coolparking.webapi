﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : Interfaces.ITimerService
    {
        private readonly Timer _timer;

        public event ElapsedEventHandler Elapsed;

        public TimerService()
        {
            _timer = new();
            _timer.AutoReset = true;
            _timer.Elapsed += Elapsed;
        }

        public double Interval
        {
            get 
            {
                return _timer.Interval;
            }
            set
            {
                _timer.Interval = value;
            }
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        public void Start()
        {
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }
    }
}